#include "GameManager.h"
#include <memory>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "skybox.h"

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}

void GameManager::privateInit()
{
    // Set default OpenGL states
    glEnable(GL_CULL_FACE);

    // Adding the camera to the scene
//    cam_.reset(new Camera());
    //  this->addSubObject(cam_);
    //     matrix_.translate(0.0f, 0.0f, -99.0f);

    skybox_.reset(new skybox());
    this->addSubObject(skybox_);

//      bf_.reset(new BattleField());
//        this->addSubObject(bf_);






}



void GameManager::privateRender()
{
    glm::mat4 m = matrix_;
    m=cam_->getMatrix();


    skybox_->setPosition(glm::vec3 (-m[3][0], -m[3][1], -m[3][2]));








}

void GameManager::privateUpdate()
{
    // Instead of adding alle objects in the scene as subobject to the camera they are added as subobjects
    // to the game manager. Therefore, we set the game manager matrix equal to the camera matrix.
   this->matrix_ = cam_->getMatrix();



}







std::shared_ptr<Camera> GameManager::getCam()
{
    return cam_;
}





