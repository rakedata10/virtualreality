#ifndef BULLET_H
#define BULLET_H






#include <parametrics/gmpsphere>

class Bullet : public GMlib::PSphere<float>
{
public:
   using PSphere::PSphere;

    ~Bullet() {}



protected:
    void localSimulate(double dt);

};


#endif // BULLET_H
