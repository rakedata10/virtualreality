#include "controller.h"


void controller::insertObject(){

    // plane..............................
    const GMlib::Point<float,3> p( -7.0, -10.0, -8.0 );
    const GMlib::Vector<float,3> V1(15.0, 0.0, 0.0);
    const GMlib::Vector<float,3> V2(0.0, 15.0, 0.0);
      Plane *plane1 = new Plane(p,V1,V2);
        plane1->toggleDefaultVisualizer();
        plane1->replot(20,20,1,1);
        this->insert(plane1);



    createRobot();
    this->setSurroundingSphere(100);

}

void controller::Robotparts(){
    const GMlib::Vector<float,3> v1head(10.0, 0.0, -10.0);
    const GMlib::Vector<float,3> v2head(10.0, 0.0, -5.0);

    // sphere(head)..............................

    GMlib::Point<float,3> Face(0,0,0);
    GMlib::Vector<float,3> v1(0,10,0);
    GMlib::Vector<float,3> v2(10,0,0);
    Plane* face_ = new Plane(Face,v1head,v2head);
    face_->toggleDefaultVisualizer();
    face_->replot(20,20,1,1);
    this->insert(face_);

    // a neck
    GMlib::Point<float,3> robotNeck(5,-1.5,0);
    auto neck = new GMlib::PCylinder<float>(0.5,0,3);
    neck->toggleDefaultVisualizer();
    neck->replot(20,20,1,1);
    neck->translate(robotNeck);
    this->insert(neck);


    //create body
    GMlib::Point<float,3> robotBody(-4,-22.5,0);
    GMlib::Vector<float,3> vec1(0,20,0);
    GMlib::Vector<float,3> vec2(20,0,0);
    Plane* body = new Plane(robotBody,vec1,vec2);
    body->toggleDefaultVisualizer();
    body->replot(20,20,1,1);
    this->insert(body);

    // (left leg)..............................

    GMlib::Point<float,3> robotLeftLeg(0,-29,0);
    auto leftLeg = new GMlib::PCylinder<float>(1,0,20);
    leftLeg->toggleDefaultVisualizer();
    leftLeg->replot(20,20,1,1);
    leftLeg->translate(robotLeftLeg);

    this->insert(leftLeg);

    // (Right leg)..............................

    GMlib::Point<float,3> robotRightLeg(10,-29,0);
    auto rightLeg = new GMlib::PCylinder<float>(1,0,20);
    rightLeg->toggleDefaultVisualizer();
    rightLeg->replot(20,20,1,1);
    rightLeg->translate(robotRightLeg);

    this->insert(rightLeg);




    // left hand.....................
    GMlib::Point<float,3> robotLeftHand(-6,-6,-1);
    auto leftHand = new GMlib::PCylinder<float>(0.5,0,10);
    leftHand->toggleDefaultVisualizer();
    leftHand->replot(20,20,1,1);
    leftHand->translate(robotLeftHand);
    leftHand->rotate(30,GMlib::Point<float,3>(0,5,0));
    this->insert(leftHand);

    //right hand......................
    GMlib::Point<float,3> robotRightHand(6,-6,-1);
    auto rightHand = new GMlib::PCylinder<float>(0.5,0,10);
    rightHand->toggleDefaultVisualizer();
    rightHand->replot(20,20,1,1);
    rightHand->translate(robotRightHand);
    rightHand->rotate(-30,GMlib::Point<float,3>(0,5,0));
    this->insert(rightHand);

}



void controller::createRobot(){

    GMlib::Point<float,3> robotBody(1,-2,1);
    body = new GMlib::PCylinder<float>(2,0,6);
    body->toggleDefaultVisualizer();
    body->replot(20,20,1,1);
    body->translate(robotBody);
    this->insert(body);

    GMlib::Point<float,3> robotHead(0,7,1);
    head = new GMlib::PSphere<float>(2);
    head->toggleDefaultVisualizer();
    head->replot(20,20,1,1);
    head->translate(robotHead);
    head->setMaterial(GMlib::GMmaterial::Silver);
    body->insert(head);

    GMlib::Point<float,3> robotNeck(0,3,1);
    neck = new GMlib::PCylinder<float>(0.5,0,3);
    neck->toggleDefaultVisualizer();
    neck->replot(20,20,1,1);
    neck->translate(robotNeck);
    body->insert(neck);




    GMlib::Point<float,3> robotleftHand(-3,8,-7);
    leftHand = new GMlib::PCylinder<float>(1,0,3);
    leftHand->toggleDefaultVisualizer();
    leftHand->replot(20,20,1,1);
    leftHand->translate(robotleftHand);
    leftHand->rotate(30,GMlib::Point<float,3>(0,5,0));
    body->insert(leftHand);

    GMlib::Point<float,3> robotrightHand(4.2,9,-8);
    rightHand = new GMlib::PCylinder<float>(0.9,0,3);
    rightHand->toggleDefaultVisualizer();
    rightHand->replot(20,20,1,1);
    rightHand->translate(robotrightHand);
    rightHand->rotate(-30,GMlib::Point<float,3>(0,8,0));
    body->insert(rightHand);

    GMlib::Point<float,3> robotleftLeg(-1.5,-1.7,-4);
    leftLeg = new GMlib::PCylinder<float>(1,0,6);
    leftLeg->toggleDefaultVisualizer();
    leftLeg->replot(20,20,1,1);
    leftLeg->translate(robotleftLeg);
    body->insert(leftLeg);

    GMlib::Point<float,3> pointrightLeg(2,-1.7,-4);
    rightLeg = new GMlib::PCylinder<float>(1,0,6);
    rightLeg->toggleDefaultVisualizer();
    rightLeg->replot(20,20,1,1);
    rightLeg->translate(pointrightLeg);
    body->insert(rightLeg);

}

/*============================================================================
 *  construction to insert the new object to the scene
=============================================================================*/
controller::controller(){
    insertObject();
    //this->setSurroundingSphere(100);
}

void controller::movelefthand(){
    //  std::cout << "Move left hand runing ...." << std::endl;

    leftHand->rotate(20,GMlib::Point<float,3>(0,5,0));
}


void controller::moverighthand(){

    rightHand->rotate(-20,GMlib::Point<float,3>(0,5,0));
}

void controller::moveright(){

    body->translate(GMlib::Vector<float,2>(0,-1));
}
void controller::moveleft(){

    body->translate(GMlib::Vector<float,2>(0,1));
}
void controller::moveforward(){

    body->translate(GMlib::Vector<float,2>(1,1));
}
void controller::movebackward(){

    body->translate(GMlib::Vector<float,2>(-1,-1));
}
void controller::firebull(){
    GMlib::Point<float,3> robotbullet(1,-2,-3);
    bullet = new Bullet(1);
    bullet->toggleDefaultVisualizer();
    bullet->replot(20,20,1,1);
    bullet->translate(robotbullet);
    bullet->setMaterial(GMlib::GMmaterial::PolishedGreen);
    leftHand->insert(bullet);
}
