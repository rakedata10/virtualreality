uniform sampler2D colorMap;
uniform sampler2D detailMap;

void main () 
{
    	// combine diffuse and specular contributions and output final vertex color

	vec2 detailCoord = vec2(gl_TexCoord[0].x * 32.0, gl_TexCoord[0].y * 512.0);
	vec4 detailColor = texture2D(detailMap, detailCoord.xy) * 0.5;

	vec2 colorCoord = gl_TexCoord[0].st;
	vec4 colorColor = texture2D(colorMap, gl_TexCoord[0].xy);

	//gl_FragColor = vec4(gl_TexCoord[0].x, gl_TexCoord[0].y, 0,0);
	//gl_FragColor = texture2D(colorMap, gl_TexCoord[0].xy);

	vec4 finalColor = (detailColor + colorColor) * 0.7;
	gl_FragColor = finalColor;
	
}