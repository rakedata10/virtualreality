uniform sampler2D tex1, tex3;
varying vec2 TexCoord;

uniform float BlendFactor;

void main()
{
	float x = 2.0;
	float z = 8.0;
	vec2 TexCoord2 = TexCoord;
	TexCoord2.x *= x;
	TexCoord2.y *= z;
	
	vec4 texel0 = texture2D(tex1, TexCoord);
    vec4 texel1 = texture2D(tex3, TexCoord2);
	
	gl_FragColor = mix(texel0, texel1, BlendFactor);
	
	

	

}


