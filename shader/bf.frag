varying vec4 texturecoord;
uniform sampler2D tex0;
uniform sampler2D tex1;

void main(){


     vec4 color = texture2D(tex0, texCoord.xy);
    gl_FragColor = 0.1*texture2D(tex1, texturecoord.xy) + 0.9*texture2D(tex0, texturecoord.xy);
}