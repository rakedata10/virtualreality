varying vec4 texturecoord;

void main(){
    texturecoord = gl_MultiTexCoord0;	
   
    vec4 pos = gl_Vertex ;
	pos.y = vec4(texture2D(detailMap, gl_MultiTexCoord0.xy)).x * 100.0f;
	gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
}