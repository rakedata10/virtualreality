uniform sampler2D heightMap;

void main()
{
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vec4 pos = gl_Vertex;

	vec4 coords = texture2D(heightMap, gl_TexCoord[0].xy);

	pos.y += coords.x * 100.0;
	pos.x *= 4.0;
	pos.z *= 2.0;

	gl_Position = gl_ModelViewProjectionMatrix * pos;
}