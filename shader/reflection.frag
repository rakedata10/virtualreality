uniform samplerCube envMap;
varying vec3  normal;
varying vec3  incident;

void main (void)
{		
	vec3 ref = reflect( normalize(incident), normalize(normal));
	gl_FragColor = textureCube(envMap, ref);	
}
