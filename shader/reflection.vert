uniform vec3  camera_pos;
varying vec3  normal;
varying vec3  incident;

void main()
{		
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	vec4 object_pos = gl_Vertex; 
	incident = vec3(object_pos.xzy - camera_pos);
	normal =  gl_NormalMatrix * gl_Normal;	
}
