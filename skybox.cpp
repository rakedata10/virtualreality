#include "skybox.h"
#include "glew.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"






skybox::skybox()
{




}

skybox::~skybox()
{

}


void skybox::privateInit(){


    int width=512;
    int length=512;
    shader.initShaders("F:/masterII/VirtualReality/shader/skybox");


    unsigned char* img = SOIL_load_image("F:/masterII/VirtualReality/image/skybox/skybox_west.bmp", &width, &length,0, SOIL_LOAD_RGB);
    unsigned char* img1 = SOIL_load_image("F:/masterII/VirtualReality/image/skybox/skybox_east.bmp", &width, &length,0, SOIL_LOAD_RGB);
    unsigned char* img2 = SOIL_load_image("F:/masterII/VirtualReality/image/skybox/skybox_up.bmp", &width, &length,0, SOIL_LOAD_RGB);
    unsigned char* img3 = SOIL_load_image("F:/masterII/VirtualReality/image/skybox//skybox_down.bmp", &width, &length,0, SOIL_LOAD_RGB);
    unsigned char* img4  = SOIL_load_image("F:/masterII/VirtualReality/image/skybox/skybox_north.bmp", &width, &length,0, SOIL_LOAD_RGB);
    unsigned char* img5 = SOIL_load_image("F:/masterII/VirtualReality/image/skybox/skybox_south.bmp", &width, &length,0, SOIL_LOAD_RGB);





    glGenTextures(1, &tex_cube);
    glActiveTexture (GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_CUBE_MAP, tex_cube);
    glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);



    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img1);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img2);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img3);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img5);

    shader.enable();

    GLuint place;
    GLuint prog = shader.getProg();

    place = glGetUniformLocation(prog,"cubemap");
    glUniform1i(place,0);
    shader.disable();




    list_id = glGenLists(1);
    float  size = 1.0;

    glNewList(list_id, GL_COMPILE);
    glBegin(GL_QUADS);


    // Near Face

    glTexCoord3f(1.0f, 0.0f,1.0f); glVertex3f( -size, -size,  size);
    glTexCoord3f(0.0f, 0.0f,1.0f); glVertex3f( size, -size,  size);
    glTexCoord3f(0.0f, 1.0f,1.0f); glVertex3f( size,  size,  size);
    glTexCoord3f(1.0f, 1.0f,1.0f); glVertex3f( -size,  size,  size);

    //Left face

    glTexCoord3f(0.0f, 0.0f,1.0f); glVertex3f(-size, -size,  size);
    glTexCoord3f(0.0f, 0.0f,0.0f); glVertex3f(-size, size, size);
    glTexCoord3f(0.0f, 1.0f,0.0f); glVertex3f(-size,  size, -size);
    glTexCoord3f(0.0f, 1.0f,1.0f); glVertex3f(-size,  -size,  -size);


    //Back face

    glTexCoord3f(0.0f, 0.0f,0.0f); glVertex3f( -size, -size,  -size);
    glTexCoord3f(1.0f, 1.0f,0.0f); glVertex3f(  -size, size, - size);
    glTexCoord3f(1.0f, 0.0f,0.0f); glVertex3f(  size, size,  -size);
    glTexCoord3f(0.0f, 1.0f,0.0f); glVertex3f( size,  -size,  -size);


    //Right face

    glTexCoord3f(1.0f, 0.0f,0.0f); glVertex3f( size, -size, -size);
    glTexCoord3f(1.0f, 0.0f,1.0f); glVertex3f( size, size,  -size);
    glTexCoord3f(1.0f, 1.0f,1.0f); glVertex3f( size,  size,  size);
    glTexCoord3f(1.0f, 1.0f,0.0f); glVertex3f( size,  -size, size);



    //Top face

    glTexCoord3f(1.0f, 1.0f,1.0f); glVertex3f( size,  size,  size);
    glTexCoord3f(0.0f, 1.0f,1.0f); glVertex3f(size,  size,  -size);
    glTexCoord3f(0.0f, 1.0f,0.0f); glVertex3f(-size,  size, -size);
    glTexCoord3f(1.0f, 1.0f,0.0f); glVertex3f( -size,  size, size);


    //Bottom face

    glTexCoord3f(1.0f, 0.0f,0.0f); glVertex3f( size, -size, size);
    glTexCoord3f(0.0f, 0.0f,0.0f); glVertex3f(-size, -size, size);
    glTexCoord3f(0.0f, 0.0f,1.0f); glVertex3f(-size, -size, -size);
    glTexCoord3f(1.0f, 0.0f,1.0f); glVertex3f( size, -size, -size);










    glEnd();
    glEndList();
}

void skybox::privateRender(){
    shader.enable();

    glFrontFace(GL_CW);


    glDisable(GL_DEPTH_TEST);

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex_cube);
    glCallList(list_id);
    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_CUBE_MAP);

    glFrontFace(GL_CCW);

    glEnable(GL_DEPTH_TEST);
    shader.disable();

}

void skybox::privateUpdate(){


}
void skybox::setPosition(const glm::vec3 &pos){

    matrix_=glm::translate(glm::mat4(),pos);
}
