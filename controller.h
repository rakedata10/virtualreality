#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "plane.h"
#include "bullet.h"
#include  "BattleField.h"
#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>
#include <parametrics/gmpsphere>
#include <parametrics/gmpplane>
#include <parametrics/gmpcylinder>
#include <parametrics/gmpcircle>
// Qt library
#include <QTimerEvent>
#include <QRectF>
#include <QMouseEvent>
#include <QDebug>
// stl library
#include <stdexcept>
#include <thread>
#include <mutex>

class controller : public GMlib::SceneObject{
    GM_SCENEOBJECT(controller)

    private:
        GMlib::PSphere<float>* head;
    GMlib::PCylinder<float>* neck;
    GMlib::PCylinder<float>* body;
    GMlib::PCylinder<float>* leftHand;
    GMlib::PCylinder<float>* rightHand;
    GMlib::PCylinder<float>* robotBody;

    GMlib::PCylinder<float>* leftLeg;
    GMlib::PCylinder<float>* rightLeg;
    Bullet* bullet;
    BattleField* plane;
public:

    controller();
    void insertObject();
    void createRobot();
    void Robotparts();
    void movelefthand();
    void moverighthand();
    void firebull();
    void moveright();
    void moveleft();
    void moveforward();
    void movebackward();
public slots:

};

#endif // CONTROLLER_H
