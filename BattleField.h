#pragma once
#include "glew.h"
#include <GL/freeglut.h>

//#include <GL/glu.h>

#include "SceneObject.h"
#include "glm/glm.hpp"
#include "Shader.h"



class BattleField : public SceneObject
{
public:
     BattleField();
     BattleField(char* path);
    ~BattleField();
    void        setVertices();
    void        setIndices();
    void        setTexture();
protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

private:
    std::vector< glm::vec3 > vertexArray_; // Maybe two-dim vector and several arrays
    std::vector< unsigned int > indexArray_;
    float _y;
    void loadImage(const char *path);
    int width, length;
    GLuint tex1;
    GLuint tex2;
    GLuint tex3;
//    GLuint img;
    int    sizex_;
    int  sizey_;
    float x_;
     int restartIndex;
    // normal array.
    // texture coord array
    std::vector< glm::vec2 > texCoordArray_;
    Shader shader;
 GLint place;
};

