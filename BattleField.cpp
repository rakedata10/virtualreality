
#include "BattleField.h"
#include "SOIL.h"



//#include "glew.h"
//#include <GL/glu.h>
//#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <iostream>
#include <QDebug>


BattleField::BattleField()
{
}


BattleField::~BattleField()
{
}
void BattleField::privateInit()
{


 shader.initShaders("F:/masterII/VirtualReality/shader/bat");

    int width = 64;
    int length = 512;

    unsigned char* img = SOIL_load_image("F:/masterII/VirtualReality/image/colorMap.bmp", &width, &length,0, SOIL_LOAD_RGB);


    glGenTextures(1, &tex1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex1);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB_FLOAT32_ATI, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img);


    unsigned char* img1 = SOIL_load_image("F:/masterII/VirtualReality/image/heightmap.png", &width, &length,0, SOIL_LOAD_RGB);

    glGenTextures(1, &tex2);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex2);



    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB_FLOAT32_ATI, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img1);



    unsigned char* img2 = SOIL_load_image("F:/masterII/VirtualReality/image/detailMap.jpg", &width, &length,0, SOIL_LOAD_RGB);

    glGenTextures(1, &tex3);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, tex3);



    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB_FLOAT32_ATI, width, length, 0, GL_RGB, GL_UNSIGNED_BYTE, img2);


shader.enable();

    GLuint prog = shader.getProg();

    place = glGetUniformLocation(prog,"tex1");
    glUniform1i(place,0);

    place = glGetUniformLocation(prog, "tex2");
    glUniform1i(place,1);

    place = glGetUniformLocation(prog, "tex3");
    glUniform1i(place,2);

    place = glGetUniformLocation(prog, "BlendFactor");
    glUniform1f(place,1);
    shader.disable();
    // Create vertex arrays and making feild




    // Creating Vertex and Texture Array
    glPrimitiveRestartIndex(32768);
    glm::vec3 v;
    glm::vec2 e;

    float sizex=64.0f;

    float sizey=256.0f;
    for(int y=0; y < sizey; y++)
    {
        for(int x=0; x < sizex; x++)
        {
            v[0]=x*10.0f-320.0;
            v[1]=0;
            v[2]=y*(-10.0f);
            e[0]=x/float(sizex-1);
            e[1]=y/float(sizey-1);
            vertexArray_.push_back(v);
            texCoordArray_.push_back(e);

        }
    }






    //     creating strips with index array

    for(int y = 0; y < sizey-1; y++){
        for(int x = 0; x < sizex; x++){

            indexArray_.push_back(x+sizex+y*sizex);
            indexArray_.push_back(x+y*(sizex));

        }

        indexArray_.push_back(32768);
    }
    indexArray_.pop_back();
}




void BattleField::privateRender()
{
   shader.enable();
    //color map

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex1);
    //height map
    glActiveTexture(GL_TEXTURE1);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex2);
       //detailmap

    glActiveTexture(GL_TEXTURE2);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex3);




    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, &vertexArray_[0]); // set vertex pointer

    glTexCoordPointer(2, GL_FLOAT, 0, &texCoordArray_[0]);
    glIndexPointer(GL_UNSIGNED_INT, 0, &indexArray_[0]);


    glEnable(GL_PRIMITIVE_RESTART);

    glDrawElements(GL_QUAD_STRIP, indexArray_.size(), GL_UNSIGNED_INT, &indexArray_[0]);

    glDisable(GL_PRIMITIVE_RESTART);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);

    glActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);

    glActiveTexture(GL_TEXTURE2);
    glDisable(GL_TEXTURE_2D);



 shader.disable();
}






void BattleField::privateUpdate()
{


}



