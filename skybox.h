#ifndef SKYBOX_H
#define SKYBOX_H

#include <windows.h>
#include "glew.h"
#include <GL/glu.h>
#include "SceneObject.h"
#include "Shader.h"
#include "SOIL.h"

class GameManager;
class skybox : public SceneObject
{
public:

    ~skybox();



    float Radius();
    skybox();
    void Position();
    void setPosition(const glm::vec3 &pos);
    void getSky();
protected:
  void privateInit();
  void privateRender();
  void privateUpdate();

private:
  int list_id;
  float size,position,radius;
  Shader skyboxshader;
  GLuint tex_cube;
  Shader shader;
};
#endif // SKYBOX_H
