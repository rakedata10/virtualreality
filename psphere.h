#ifndef PSPHERE_H
#define PSPHERE_H


#include <parametrics/gmpplane>


class TestPlane : public GMlib::PPlane<float> {
public:
    using PPlane::PPlane;

const GMlib::Point<float,3>& getCornerPoint();


};

 // END namespace GMlib

#endif // PSPHERE_H
