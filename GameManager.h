#pragma once

#include <windows.h>
#include "glew.h"
#include "Camera.h"
#include <GL/glu.h>
#include "BattleField.h"
#include "SceneObject.h"




#include "skybox.h"

class GameManager : public SceneObject
{
public:
    GameManager();
    ~GameManager();

    //    void addBullet(glm::vec3 pos, int type);

   std::shared_ptr<Camera> getCam();
//    std::shared_ptr<SpaceShip> getShip();
//    std::shared_ptr<Enemy> getEnemy();

    void addBullet();
protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

private:
   std::shared_ptr<BattleField> bf_;
//    std::shared_ptr<SpaceShip> spaceship_;
   std::shared_ptr<Camera> cam_;

//    std::vector< std::shared_ptr<Enemy> > enemies_;
//    std::shared_ptr<Enemy>  enemy_;

//    std::shared_ptr<Bullet> bullet_;
//    std::vector<std::shared_ptr<Bullet> > bullets_;
    std::shared_ptr<skybox>  skybox_;
//    std::shared_ptr<water> water_;
    float m;

    int type;
    glm::vec3 pos_;
    glm::vec3 pos;
GLuint tex;

};

